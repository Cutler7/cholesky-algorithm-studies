#include <stdio.h>

#ifndef _NR_UTILS_H_
#define _NR_UTILS_H_

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

#if defined(__STDC__) || defined(ANSI) || defined(NRANSI) /* ANSI */

void nrerror(char error_text[]);

int *ivector(long nl, long nh);
void free_ivector(int *v, long nl, long nh);

double *dvector(long nl, long nh);
void free_dvector(double *v, long nl, long nh);

double **dmatrix(long nrl, long nrh, long ncl, long nch);
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch);



int ludcmp(double**, int, int*, double*);

void lubksb(double**, int, int*, double[]);
/*
int load_file(FILE*, double**, double*, int*, double*, int, int);

FILE* load_file_header(FILE*, int, char**, char[255], int*, int*);
*/
int print_vector(double*, int, int, char[]);


/*
float *vector(long nl, long nh);
void free_vector(float *v, long nl, long nh);

float **matrix(long nrl, long nrh, long ncl, long nch);
void free_matrix(float **m, long nrl, long nrh, long ncl, long nch);

int **imatrix(long nrl, long nrh, long ncl, long nch);
void free_imatrix(int **m, long nrl, long nrh, long ncl, long nch);
*/

#else /* ANSI */
/* traditional - K&R */

void nrerror();

double *dvector();
void free_dvector();

double **dmatrix();
void free_dmatrix();

int *ivector();
void free_ivector();

int ludcmp();

void lubksb();
int print_vector();
/*
float *vector();
void free_vector();

float **matrix();
void free_matrix();

int **imatrix();
void free_imatrix();
*/

#endif /* ANSI */

#endif /* _NR_UTILS_H_ */
