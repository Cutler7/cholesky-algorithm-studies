#include "nrutil2.h"
#include "uklad-rownan.h"

void wyznacz_predyktor_ls(int rzad_temp, mType **iloczyny3, mType *iloczyny3_P, mType *wsp_ls) {

    mType **matrix1, *vector1, *vector3;
    int *vector2;
    int blad_rownania, i, j;

    //Alokacja macierzy
    matrix1 = dmatrix(1, rzad_temp, 1, rzad_temp); //Macierz wejsciowa
    vector1 = dvector(1, rzad_temp); //wektor wejsciowy (prawostronny)
    vector2 = ivector(1, rzad_temp); //wektor indeksow
    vector3 = dvector(1, rzad_temp); //wektor wynikowy

    //Kopiowanie tab. iloczynow
    for (j = 0; j < rzad_temp; j++) {
        vector3[j + 1] = iloczyny3_P[j];
        for (i = 0; i < rzad_temp; i++)
            matrix1[j + 1][i + 1] = iloczyny3[j][i];
    }
    //Rozwiazanie
    blad_rownania = 0;
    blad_rownania = ludcmp(matrix1, rzad_temp, vector2, vector1);
    if (blad_rownania == 0) lubksb(matrix1, rzad_temp, vector2, vector3);

    //Zapis wynikow (wspolczynnikow predykcji)
    for (j = 0; j < rzad_temp; j++)
        wsp_ls[j] = vector3[j + 1];

    //Czyszczenie
    free_dvector(vector1, 1, rzad_temp);
    free_ivector(vector2, 1, rzad_temp);
    free_dvector(vector3, 1, rzad_temp);
    free_dmatrix(matrix1, 1, rzad_temp, 1, rzad_temp);
}
