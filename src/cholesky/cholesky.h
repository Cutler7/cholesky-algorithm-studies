//
// Created by Daniel on 19.06.2020.
//

#ifndef CHOLESKY_CHOLESKY_H
#define CHOLESKY_CHOLESKY_H

#include <aliased-types.h>

int choldc(mType **a, int n, mType p[]);

void cholsl(mType **a, int n, mType p[], mType b[], mType x[]);

#endif //CHOLESKY_CHOLESKY_H
