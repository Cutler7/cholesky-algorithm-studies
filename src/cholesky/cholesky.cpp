//
// Created by Daniel on 19.06.2020.
//

#include "cholesky.h"
#include <math.h>

mType getMEl(mType **a, int x, int y) {
    return a[x - 1][y - 1];
}

void setMEl(mType **a, int x, int y, mType val) {
    a[x - 1][y - 1] = val;
}

mType getVEl(mType *a, int x) {
    return a[x - 1];
}

void setVEl(mType *a, int x, mType val) {
    a[x - 1] = val;
}

int choldc(mType **a, int n, mType *p) {
    int i, j, k;
    mType sum;
    for (i = 1; i <= n; i++) {
        for (j = i; j <= n; j++) {
            for (sum = getMEl(a, i, j), k = i - 1; k >= 1; k--) sum -= getMEl(a, i, k) * getMEl(a, j, k);
            if (i == j) {
                if (sum <= 0.0)
                    return 1;
                setVEl(p, i, sqrt(sum));
            } else setMEl(a, j, i, sum / getVEl(p, i));
        }
    }

    for (i = 1; i <= n; i++) {
        setMEl(a, i, j, 1.0 / getVEl(p, i));
        for (j = i + 1; j <= n; j++) {
            sum = 0.0;
            for (k = i; k < j; k++) sum -= getMEl(a, j, k) * getMEl(a, k, i);
            setMEl(a, j, i, sum / getVEl(p, j));
        }
    }
    return 0;
}

void cholsl(mType **a, int n, mType *p, mType *b, mType *x) {
    int i, k;
    mType sum;
    for (i = 1; i <= n; i++) { // Solve L · y = b, storing y in x.
        for (sum = getVEl(b, i), k = i - 1; k >= 1; k--) sum -= getMEl(a, i, k) * getVEl(x, k);
        setVEl(x, i, sum / getVEl(p, i));
    }
    for (i = n; i >= 1; i--) { // Solve LT · x = y.
        for (sum = getVEl(x, i), k = i + 1; k <= n; k++) sum -= getMEl(a, k, i) * getVEl(x, k);
        setVEl(x, i, sum / getVEl(p, i));
    }
}
