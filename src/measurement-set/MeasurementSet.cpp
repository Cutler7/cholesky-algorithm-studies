//
// Created by Daniel on 19.06.2020.
//

#include <iostream>
#include <uklad-rownan.h>
#include <cholesky.h>
#include <parseProgramArguments.h>
#include "MeasurementSet.h"
#include <chrono>

using namespace std;

MeasurementSet::MeasurementSet(int matrixSize, const RegisteredParams &params) {
    this->matrixSize = matrixSize;
    this->shortLog = params.shortLog;
    this->choleskyOnly = params.choleskyOnly;
    this->controlOnly = params.controlOnly;
    this->attemptNumber = params.attemptNumber;
    this->runBoth = params.choleskyOnly == params.controlOnly;
    this->choleskySet = new MatrixGenerator(matrixSize, attemptNumber);
    this->controlSet = new MatrixGenerator(*(this->choleskySet));
}

int MeasurementSet::executeCalculation() {
    this->calculationPerformed = true;
    if (this->runBoth || this->choleskyOnly) {
        this->choleskyTimeStart = Clock::now();
        this->calculateCholesky();
        this->choleskyTimeEnd = Clock::now();
    }
    if (this->runBoth || this->controlOnly) {
        this->controlTimeStart = Clock::now();
        this->calculateControlGroup();
        this->controlTimeEnd = Clock::now();
    }
    return 0;
}

int MeasurementSet::calculateCholesky() {
    mType *buffer = new mType[this->matrixSize];
    for (int i = 0; i < this->attemptNumber; i++) {
        choldc(
                this->choleskySet->getMatrixNo(i),
                this->matrixSize,
                buffer
        );
        cholsl(
                this->choleskySet->getMatrixNo(i),
                this->matrixSize,
                buffer,
                this->choleskySet->getVectorNo(i),
                this->choleskySet->getVectorNo(i)
        );
    }
    delete[] buffer;
    return 0;
}

int MeasurementSet::calculateControlGroup() {
    for (int i = 0; i < this->attemptNumber; i++) {
        wyznacz_predyktor_ls(
                this->matrixSize,
                this->controlSet->getMatrixNo(i),
                this->controlSet->getVectorNo(i),
                this->controlSet->getVectorNo(i)
        );
    }
    return 0;
}

void MeasurementSet::checkIsCalculationPerformed() {
    if (!calculationPerformed) {
        throw runtime_error("Calculation not performed!");
    }
}

void MeasurementSet::getExecutionInfo() {
    this->checkIsCalculationPerformed();
    long choleskyTimeTaken = chrono::duration_cast<std::chrono::milliseconds>(this->choleskyTimeEnd - this->choleskyTimeStart).count();
    long controlTimeTaken = chrono::duration_cast<std::chrono::milliseconds>(this->controlTimeEnd - this->controlTimeStart).count();
    if (this->shortLog) {
        this->printShortLog(choleskyTimeTaken, controlTimeTaken);
    } else {
        this->printFullLog(choleskyTimeTaken, controlTimeTaken);
    }
}

void MeasurementSet::printFullLog(long choleskyTime, long controlTime) {
    cout << "----------------------" << endl;
    cout << "Matrix size: " << this->matrixSize << endl;
    cout << "Attempt number: " << this->attemptNumber << endl;
    cout << "CHOLESKY: " << endl;
    cout << "total: " << choleskyTime << endl;
    cout << "avg: " << (double)choleskyTime / this->attemptNumber << endl;
    cout << "CONTROL: " << endl;
    cout << "total: " << controlTime << endl;
    cout << "avg: " << (double)controlTime / this->attemptNumber << endl;
    cout << "----------------------\n\n" << endl;
}

void MeasurementSet::printShortLog(long choleskyTime, long controlTime) {
    cout << this->matrixSize << "," << choleskyTime << "," << (double)choleskyTime / this->attemptNumber << ","
         << controlTime << "," << (double)controlTime / this->attemptNumber << endl;
}
