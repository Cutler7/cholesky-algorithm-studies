//
// Created by Daniel on 18.06.2020.
//

#ifndef CHOLESKY_MATRIXGENERATOR_H
#define CHOLESKY_MATRIXGENERATOR_H

#include <aliased-types.h>

class MatrixGenerator {

    int listLen;
    int size;
    mType ***matrix;
    mType **vector;

public:
    MatrixGenerator(int size, int listLen);
    MatrixGenerator(const MatrixGenerator &source);
    ~MatrixGenerator();
    mType ** getMatrixNo(int no) const;
    mType *getVectorNo(int no) const;

private:
    void allocMemory();
    void generateRandomVectors();
    void generateRandomMatrices();
    void copyMatrixData(const MatrixGenerator &source);
    mType getRandomNumber();
};

#endif //CHOLESKY_MATRIXGENERATOR_H
