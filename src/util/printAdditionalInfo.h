//
// Created by Daniel on 19.06.2020.
//

#ifndef CHOLESKY_PRINTADDITIONALINFO_H
#define CHOLESKY_PRINTADDITIONALINFO_H

#include_next <iostream>

using namespace std;

void printPrologue(bool clearLog) {
    if (!clearLog) {
        cout << "EXECUTION LOG PRINT:\n" << endl;
    }
}

void printEpilogue(bool clearLog) {
    if (!clearLog) {
        cout << "- program finished -" << endl;
    }
}

#endif //CHOLESKY_PRINTADDITIONALINFO_H
