//
// Created by Daniel on 19.06.2020.
//

#ifndef CHOLESKY_PARSEPROGRAMARGUMENTS_H
#define CHOLESKY_PARSEPROGRAMARGUMENTS_H

#include <iostream>
#include <unistd.h>

#define MAX_ARG_NUMBER 8

using namespace std;

struct registeredParams {
    bool shortLog{};
    bool choleskyOnly{};
    bool controlOnly{};
    int lowerRange{};
    int upperRange{};
    int step{};
    int attemptNumber{};
};

typedef struct registeredParams RegisteredParams;

RegisteredParams parseProgramArguments(int argc, char *argv[]);

#endif //CHOLESKY_PARSEPROGRAMARGUMENTS_H
