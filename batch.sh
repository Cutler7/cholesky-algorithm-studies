#!/bin/bash

LOGFILE=../log/results.txt
LOWER=20
UPPER=300
STEP=20
COUNT=1000

function print_header() {
  echo "============================"
  echo "<AUTOMATION SCRIPT>"
  echo "output in: [project-root]/log/results.txt"
  echo $'============================\n'
}

cd cmake-build-debug
true > "$LOGFILE"
print_header

# [lower_limit] [upper_limit] [step] [attempt_number]
echo "${LOWER},${UPPER},${STEP},${COUNT}" > "$LOGFILE"
./cholesky -sh $LOWER $UPPER $STEP $COUNT | tee -a $LOGFILE

printf '\nFinished!\n'
